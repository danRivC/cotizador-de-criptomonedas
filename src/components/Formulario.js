import React, {useEffect, useState} from 'react';
import styled from '@emotion/styled';
import useMoneda from '../hooks/useMoneda';
import useCriptomoneda from '../hooks/useCriptomoneda';
import axios from 'axios';
const Boton = styled.input`
    margin-top:20px;
    font-weight:bold;
    font-size:20px;
    padding: 10px;
    background-color:#66a2fe;
    border:none;
    width:100%;
    border-radius:10px;
    color:#FFF;
    transition:background-color .3s ease;

    &:hover{
        background-color:#326AC0;
        cursor:pointer;
    }


`

const Formulario = () =>{

    // State del listado de Criptomonedas

    const [listaCripto, guardarCriptoMonedas]=useState([]);
    const [error, guardarError]= useState(false);


    const MONEDAS=[
        {codigo:'USD', nombre:'Dolar'},
        {codigo:'MXN', nombre:'Peso Mexicano'},
        {codigo:'EUR', nombre:'Euro'},
        {codigo:'GBP', nombre:'Libra esterlina'}
    ]
    //  Utilizar useMoneda
    const [moneda, SelectMonedas]=useMoneda('Selecciona tu moneda', '', MONEDAS);
    const [ criptomoneda, SelectCripto, ]= useCriptomoneda('Elije tu criptomoneda', '', listaCripto);


    //Ejecutar LLamado a la API

    useEffect(()=>{
        const consultarApi = async()=>{
            const url = `https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD`;
            const resultado = await axios.get(url)
            guardarCriptoMonedas(resultado.data.Data);
        }
        consultarApi();

    }, [])
    //cuando se hace submit
    const cotizarMoneda = e=>{
        e.preventDefault();

        // validar si ambos campos estan llenos
        if( moneda === '' || criptomoneda ===''){
            guardarError(true)
            return
        }
        guardarError(false);
    } 

    return(
        <form onSubmit={cotizarMoneda}>
            {error?'Hay un error':''}
            <SelectMonedas></SelectMonedas>
            <SelectCripto></SelectCripto>
            <Boton 
                type="submit" 
                value="Calcular"
            />
        </form>
    );

}
export default Formulario